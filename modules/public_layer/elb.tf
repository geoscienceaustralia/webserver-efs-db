#==============================================================
# Public / elb.tf
#==============================================================

# Create an Elastic Load Balancer to route traffic to our
# Autoscaling group, and health check the app server.

#--------------------------------------------------------------
# ACM Certificate
#--------------------------------------------------------------

# Add your Domain and uncomment this block when you have issued 
# a certificate. It will find your certificate so Terraform can 
# Reference it.

# data "aws_acm_certificate" "certificate" {
#   domain   = "ENTERYOURDNSNAMEHERE.com"
#   statuses = ["ISSUED"]
# }

resource "aws_elb" "elb" {
  name    = "${var.stack_name}-${var.environment}-elb"
  subnets = aws_subnet.public.*.id

  listener {
    instance_port     = var.instance_port
    instance_protocol = var.instance_protocol
    lb_port           = "80"
    lb_protocol       = "HTTP"
  }

  #--------------------------------------------------------------
  # HTTPS Listener
  #--------------------------------------------------------------


  # Uncomment this block when you have issued a certificate


  # listener {
  #   ssl_certificate_id = data.aws_acm_certificate.certificate.arn
  #   instance_port      = var.instance_port
  #   instance_protocol  = var.instance_protocol
  #   lb_port            = "443"
  #   lb_protocol        = "HTTPS"
  # }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 30

    target = var.healthcheck

    interval = 60
  }
  security_groups = [aws_security_group.elb_sg.id]
  tags = {
    Name       = "${var.stack_name}-${var.environment}-elb"
    owner      = var.owner
    stack_name = var.stack_name
    created_by = "terraform"
  }
}
