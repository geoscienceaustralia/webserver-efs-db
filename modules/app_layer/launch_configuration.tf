#==============================================================
# App / launch-configuration.tf
#==============================================================

# Application server

data "template_file" "userdata" {
  # Treat the userdata like a template so we can load TF variables into it
  template = file(var.userdata_filepath)

  # Variables are definined as ${variable} in the .tpl file
  vars = {
    efs_id            = aws_efs_file_system.efs.id
    db_admin_username = var.db_admin_username
    db_admin_password = var.db_admin_password
    db_database_name  = var.db_database_name
    db_user_username  = var.db_user_username
    db_user_password  = var.db_user_password
    db_endpoint       = var.db_endpoint
    db_port           = var.db_port
    efs_backup_bucket = var.efs_backup_bucket
  }
}

resource "aws_launch_configuration" "lc" {
  lifecycle {
    create_before_destroy = true
  }

  image_id      = var.asg_amis
  instance_type = var.instance_type

  security_groups = [
    aws_security_group.app_sg.id,
  ]

  user_data                   = data.template_file.userdata.rendered
  key_name                    = var.key_name
  associate_public_ip_address = false

  iam_instance_profile = aws_iam_instance_profile.efs_backup.id
}
