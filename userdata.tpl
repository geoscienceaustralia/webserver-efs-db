#!/bin/bash

#==============================================================
# userdata.tpl
#==============================================================

# This is a template that is used to generate the userdata.sh
# that will be run on the application server when it is built.
# The variables are replaced during a terraform apply.

#--------------------------------------------------------------
# Environment Variables
#--------------------------------------------------------------

# Set the variables to the values passed in by bitbucket.
# Environment variables in the form TF_VAR_var_name are 
# inserted into this file by terraform.

export MYSQL_ROOT_USER=${db_admin_username}
export MYSQL_ROOT_PWD=${db_admin_password}
export DATABASE_USER=${db_user_username}
export DATABASE_USER_PWD=${db_user_password}
export DATABASE_NAME=${db_database_name}
export RDS_ENDPOINT=${db_endpoint}
export DB_PORT=${db_port}

#we want these to be inserted into our script.
#${efs_backup_bucket}

#--------------------------------------------------------------
# Install Packages
#--------------------------------------------------------------

# In a production system this would be baked into the AMI, 
# Look into our packer examples to see how this can be done.

# Install Apache (webserver) and nfs-common (used to mount the efs)
sudo apt-get -y update 
sudo apt-get install -y nfs-common apache2 awscli

#--------------------------------------------------------------
# Elastic File System
#--------------------------------------------------------------

# Remove existing default sites so we can mount the efs in its place
sudo rm -rf /var/www/html
sudo mkdir /var/www/html

# Mount the EFS
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${efs_id}.efs.ap-southeast-2.amazonaws.com:/ /var/www/html/

#--------------------------------------------------------------
# Configure Database
#--------------------------------------------------------------

# The database is only configured the first time
# the stack is created. 

# Test if database already exists
RESULT=`mysqlshow --user=$MYSQL_ROOT_USER --password=$MYSQL_ROOT_PWD --host=$RDS_ENDPOINT --port=$DB_PORT $DATABASE_NAME 2>/dev/null | grep Database | awk '{print $2;}'`
if [ "$RESULT" == $DATABASE_NAME ]; then
    echo 'Database already exists - skipping creation...'
    # Need to chown the dir so its user is the same as our user
    sudo chown -R www-data:www-data /var/www/html 
else
    # Create app database
    echo 'Creating database...'
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT -e "CREATE DATABASE $DATABASE_NAME;"

    # Clean up environment variable interpolation - create a temporary file for the GRANT SQL command
    echo "GRANT ALL ON DATABASE_NAME.* to 'DATABASE_USER' IDENTIFIED BY 'DATABASE_USER_PWD';" > ~/grant.sql
    sed -i "s/DATABASE_NAME/$DATABASE_NAME/" ~/grant.sql
    sed -i "s/DATABASE_USER/$DATABASE_USER/" ~/grant.sql
    sed -i "s/DATABASE_USER_PWD/$DATABASE_USER_PWD/" ~/grant.sql
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT < ~/grant.sql
    rm ~/grant.sql

    # Flush privileges
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT -e "FLUSH PRIVILEGES;"

    #--------------------------------------------------------------
    # Configure Webpage
    #--------------------------------------------------------------

    # Create a simple web page to prove apache works

    echo "<h1>Hello World</h1>" | sudo tee -a /var/www/html/index.html

fi 

#--------------------------------------------------------------
# Backup script
#--------------------------------------------------------------
aws configure set s3.signature_version s3v4
cd /tmp

cat << 'EOF' >> efs-backup.sh
#!/bin/bash

#==============================================================
# backup-efs.sh
#==============================================================

# This script will create a tarball of the efs directory and
# upload it to an s3 bucket (specified during the terraform build)

export LAST_BACKUP=`date +\%F`

# Create Tarball
cd /var/www/html
sudo touch "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz
sudo tar -cvzf "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz . --exclude="$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz

# Upload to bucket (n.b. the bucket name is inserted during terraform apply)
aws s3api put-object --server-side-encryption AES256 --body "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz --key "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz --bucket "${efs_backup_bucket}"

sudo rm -f "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz
EOF

# Move backup script to an executable place
sudo mv -f /tmp/efs-backup.sh /usr/local/bin/efs-backup.sh
sudo chmod +x /usr/local/bin/efs-backup.sh

# create cron job to backup nightly
crontab -l >mycron
# 1 am AEST
echo "00 15 * * * efs-backup.sh" >> mycron
crontab mycron
rm mycron

#--------------------------------------------------------------
# Cleanup Environment Variables
#--------------------------------------------------------------

# remove variables
unset MYSQL_ROOT_USER
unset MYSQL_ROOT_PWD
unset DATABASE_USER
unset DATABASE_USER_PWD
unset DATABASE_NAME
unset RDS_ENDPOINT
unset DB_PORT
