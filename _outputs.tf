#==============================================================
# outputs.tf
#==============================================================

# This file defines the information we want to write to the
# console after a succesful `terraform apply`

output "elb_dns_name" {
  description = "The dns name of the website"
  value       = module.public.elb_dns_name
}

output "database_fqdn" {
  description = "The dns name of the database (internal only)"
  value       = module.dns.fqdn
}
