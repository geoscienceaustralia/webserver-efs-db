#==============================================================
# App / iam_instance_profile.tf
#==============================================================

# Create an instance profile to allow our server to upload
# backups to an s3 bucket

# The Profile we attach to the launch config
resource "aws_iam_instance_profile" "efs_backup" {
  name = "${var.stack_name}-${var.environment}-instance-profile"
  role = aws_iam_role.efs_backup_role.id
}

# The policy to allow access to the bucket
resource "aws_iam_role_policy" "efs_backup_policy" {
  name = "${var.stack_name}-${var.environment}-s3-policy"
  role = aws_iam_role.efs_backup_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject*",
        "s3:GetObject*"
      ],
      "Resource": ["arn:aws:s3:::${var.efs_backup_bucket}/*"]
    }
  ]
}
EOF
}

# The Role itself
resource "aws_iam_role" "efs_backup_role" {
  name = "${var.stack_name}-${var.environment}efs-backup-role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
