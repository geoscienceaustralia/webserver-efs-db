#==============================================================
# App / efs.tf
#==============================================================

# Elastic File System to host the app files

resource "aws_efs_file_system" "efs" {
  tags = {
    Name        = "${var.stack_name}-${var.environment}-efs"
    owner       = var.owner
    environment = var.environment
    stack_name  = var.stack_name
  }
}

resource "aws_efs_mount_target" "efs_mount_target" {
  count           = length(var.availability_zones)
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = element(aws_subnet.private.*.id, count.index)
  security_groups = [aws_security_group.mount_target_sg.id]
}

resource "aws_security_group" "mount_target_sg" {
  name        = "${var.stack_name}-${var.environment}-inbound-nfs"
  description = "Allow NFS (EFS) access inbound"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.app_sg.id]
  }

  tags = {
    Name        = "${var.stack_name}-${var.environment}-inbound-nfs"
    managed_by  = "Terraform"
    owner       = var.owner
    environment = var.environment
    stack_name  = var.stack_name
  }
}
