#==============================================================
# Database / outputs.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

variable "database_subnet_cidr" {
  default     = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
  description = "List of subnets to be used for databases"
}

variable "availability_zones" {
  type = map
}

variable "app_sg_id" {}

variable "vpc_id" {}

#--------------------------------------------------------------
# Database
#--------------------------------------------------------------

variable "identifier" {
  default     = "mydb-rds"
  description = "Identifier for your DB"
}

variable "storage" {
  default     = "10"
  description = "Storage size in GB"
}

variable "engine" {
  default     = "mysql"
  description = "Engine type, example values mysql, postgres"
}

variable "engine_version" {
  description = "Engine version"

  default = {
    mysql = "5.6.34"
  }
}

variable "instance_class" {
  default     = "db.t2.micro"
  description = "Instance class"
}

variable "db_name" {
  default     = "mydb"
  description = "db name"
}

variable "username" {
  default     = "myuser"
  description = "User name"
}

variable "password" {
  description = "password, provide through your ENV variables"
  default     = "Password123"
}

variable "rds_is_multi_az" {
  default = "false"
}

variable "backup_retention_period" {
  # Days
  default = "30"
}

variable "backup_window" {
  # 12:00AM-03:00AM AEST
  default = "14:00-17:00"
}

variable "storage_encrypted" {
  default = false
}

variable "db_port_num" {}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------

variable "stack_name" {}

variable "environment" {}

variable "owner" {}
