#==============================================================
# Public / outputs.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

variable "region" {}

variable "nat_gw_count" {
  default     = "3"
  description = "Number of NAT gateway instances"
}

variable "availability_zones" {
  type = map
}

variable "public_subnet_cidr" {
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  description = "List of public subnets"
}

variable "vpc_id" {}

variable "igw_id" {}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------

variable "stack_name" {}

variable "environment" {}

variable "owner" {}

#--------------------------------------------------------------
# Elastic Load Balancer
#--------------------------------------------------------------

variable "elb_check_path" {
  default = "/"
}

variable "instance_port" {}

variable "instance_protocol" {}

variable "healthcheck" {}

variable "http_ip_address" {
  type = map
}
