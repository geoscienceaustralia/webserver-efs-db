#==============================================================
# App / variables.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

variable "private_subnet_cidr" {
  default     = ["10.0.51.0/24", "10.0.52.0/24", "10.0.53.0/24"]
  description = "List of private subnets"
}

variable "availability_zones" {
  type = map
}

variable "vpc_id" {}

variable "port_num" {
  default = "80"
}

#--------------------------------------------------------------
# Application Server
#--------------------------------------------------------------

variable "asg_amis" {}

variable "instance_type" {
  default     = "t2.micro"
  description = "Default instance type"
}

variable "elb_sg_id" {}

variable "userdata_filepath" {
  default = "./userdata.tpl"
}

variable "key_name" {}

#--------------------------------------------------------------
# Userdata Script
#--------------------------------------------------------------

variable "db_admin_username" {}

variable "db_admin_password" {}

variable "db_user_username" {}

variable "db_user_password" {}

variable "db_database_name" {}

variable "db_endpoint" {}

variable "db_port" {}

variable "efs_backup_bucket" {}

#--------------------------------------------------------------
# Autoscaling Group
#--------------------------------------------------------------

variable "asg_min" {
  default     = "1"
  description = "Minimum number of instances"
}

variable "asg_max" {
  default     = "1"
  description = "Maximum number of instances"
}

variable "elb_name" {}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------

variable "stack_name" {}

variable "environment" {}

variable "owner" {}

variable "ngw_ids" {
  type = list(string)
}
