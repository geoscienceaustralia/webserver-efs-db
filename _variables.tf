#==============================================================
# variables.tf
#==============================================================

# This file is used to set variables that are passed to sub 
# modules to build our stack

#--------------------------------------------------------------
# Terraform Remote State
#--------------------------------------------------------------

# Define the remote objects that terraform will use to store
# state. We use a remote store, so that you can run destroy
# from a seperate machine to the one it was built on.

terraform {
  required_version = ">= 0.12.1"

  backend "s3" {
    # This is an s3bucket you will need to create in your aws 
    # space
    bucket = "ga-devs-tfstate"

    # The key should be unique to each stack, because we want to
    # have multiple enviornments alongside each other we set
    # this dynamically in the bitbucket-pipelines.yml with the
    # --backend
    key = "three-tier-efs-dev"

    region = "ap-southeast-2"

    # This is a DynamoDB table with the Primary Key set to LockID
    lock_table = "terraform-lock"

    #Enable server side encryption on your terraform state
    encrypt = true
  }
}

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Variables used in the global config

variable "region" {
  description = "The AWS region we want to build this stack in"
  default     = "ap-southeast-2"
}

variable "stack_name" {
  description = "The name of our application"
  default     = "three-tier-efs"
}

variable "owner" {
  description = "A group email address to be used in tags"
  default     = "autobots@ga.gov.au"
}

variable "environment" {
  description = "Used for seperating terraform backends and naming items"
  default     = "dev"
}

variable "availability_zones" {
  description = "Geographically distanced areas inside the region"

  default = {
    "0" = "ap-southeast-2a"
    "1" = "ap-southeast-2b"
    "2" = "ap-southeast-2c"
  }
}

variable "key_name" {
  description = "AWS EC2 Keypair to be configured on the servers"
  default     = "INSERT_YOUR_KEYPAIR_HERE"
}

#--------------------------------------------------------------
# Database
#--------------------------------------------------------------

# Variables used in the database config

variable "db_name" {
  description = "The name of our rds"
  default     = "appdatabase"
}

variable "rds_is_multi_az" {
  description = "Create backup database in seperate availability zone"
  default     = "false"
}

variable "db_port" {
  description = "The port the Application Server will access the database on"
  default     = 3306
}

#--------------------------------------------------------------
# Userdata
#--------------------------------------------------------------

# Variables used in the userdata.sh script
# These are set in bitbucket and have no default value.
# Environment variables with the format TF_VAR_var_name will be
# used by terraform as the value of these variables.

variable "db_user_username" {
  description = "The username the app will use to access the database"
}

variable "db_user_password" {
  description = "The password of the app user"
}

variable "db_database_name" {
  description = "The name of our app database"
}

variable "db_admin_username" {
  description = "The name of our app database root account"
}

variable "db_admin_password" {
  description = "The password of our app database root account"
}

variable "ssh_ip_address" {
  description = "The address that the Jumpbox will accept SSH from"
  default     = "0.0.0.0/0"
}

variable "http_ip_address" {
  description = "Lock down http/s to this address when environment is dev / test"

  default = {
    "dev"  = "0.0.0.0/0"
    "test" = "0.0.0.0/0"
    "prod" = "0.0.0.0/0"
  }
}

#--------------------------------------------------------------
# Server Images
#--------------------------------------------------------------

# Search for our server images
# If this fails you will need to build it in your space

data "aws_ami" "ga_ubuntu" {
  most_recent = true
  name_regex  = "^Geoscience Australia Standard Apache Image.*"
  owners      = ["121695370940"]
}
